<?php

use Swoole\WebSocket\Server;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;



$host = 'localhost';
$port = 9508 ; 
$server = new Server($host, $port);

$server->on("start", function (Server $server) use($host , $port) {
	echo "Swoole WebSocket Server is started at {$host}:{$port}\n";
});

$server->on('open', function(Server $server, Swoole\Http\Request $request)  {
	echo "connection open: {$request->fd}\n";
	
});
$server->on('close', function(Server $server, int $fd) {
    echo "connection close: {$fd}\n";
});



function goSearch($column , $searchFor ) 
{

$servername = "db";
$username = "root";
$password = "sigurnaparola";


try {
	$pdo = new PDO("mysql:host=$servername;dbname=test", $username, $password);
	// set the PDO error mode to exception
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {

}
$comparisonOperator = ''; 
$query= '';
if($column=='id')
{
	$query = "select * from it where id = {$searchFor}";
}
elseif($column=='search')
{
	$searchFor = trim($searchFor);
	$searchKeyWords = explode(' ' , $searchFor); 
	$fullTextSearch = '';
	foreach($searchKeyWords as $individualKeyWord)
	{
		$fullTextSearch.="+{$individualKeyWord} ";
	}
	$fullTextSearch = trim($fullTextSearch);
	
	//$query= "select * from it where search like '%{$searchFor}%' ;";
	$query =<<<QUERY
	SELECT *
	FROM it
	WHERE MATCH(search) AGAINST('{$fullTextSearch}' IN BOOLEAN MODE );
QUERY;
}


$stmt = $pdo->query($query);
$resultsNumber = $stmt->rowCount();
$output = '<div>Намерен e повече от 1 резулатат </div>';
if($resultsNumber > 1 )
{
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	foreach($result as $individual)
	{
		$output.="<button onclick=\"searchBy('id' , {$individual->id})\">{$individual->search}</button>";
	}
	return $output;
}
elseif($resultsNumber == 1 ){
$result = $stmt->fetch(PDO::FETCH_OBJ);
$valueToReturn = $result->result ;
$edit = <<<EDIT
<a href="teachMeSomething.php?id={$result->id}" target="_blank"><img src="images/edit.png" ></a>
EDIT;
$valueToReturn.=$edit;

return $valueToReturn ; 
}
elseif($resultsNumber == 0)
{
	return 'Няма намерени резултати';
}

}

$server->on('message', function(Server $server, Frame $frame)  {
	
	$messageFromClient  = json_decode($frame->data);
	
	
	$server->push($frame->fd , goSearch($messageFromClient->searchByColumn , $messageFromClient->searchFor) ) ;
	
	
	

});



$server->start();

?>
