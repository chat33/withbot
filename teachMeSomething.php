<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<style>
		*{
				margin: 0;
				padding: 0;
				box-sizing: border-box;
			}
			body{
				display: flex;
				justify-content: center;
				align-items: center;
				min-height: 100vh;
				width: 100vw;
			
			}
			#wrapper{
				
				position: relative;
				width: 80%;
				padding: 10px 0 0 10px;
				background-color: #8d93b5;
			}
			#wrapper > div{
				margin-top:10px;
				margin-bottom: 10px;
			}
			#textarea1{
				width: 50%;
				height: 100px;
				padding: 10px;
				font-size: 16px;	
			}
			#textarea2 , #mirror{
				position: absolute;
				top: 0;
				left: 0;		
				display: block;
				width: 90%;
				height: 400px;
				padding: 10px;
				font-size: 16px;
				font-family: Chandas ;
				background-color: white;
				overflow: scroll;
				border: none;
				margin-top: 0;
				white-space: break-spaces;
				
			}
			#textarea2{
				z-index: 1000;
			}
			#mirror{
				z-index: 1;
				
			}
			
			#textarea2wrapper{
				width: auto;
				position: relative;
				height: 400px;
				
			}
			#save{
				cursor: pointer;
			}
			#categoryWrapper{
				display: flex;
				gap: 20px;
			}
			#categoryWrapper div{
				cursor: pointer;
				border-radius: 5px;
				border: 1px solid white;
				padding: 10px;
			}
			#categoryWrapper div:hover{
			color: white;
			}
			#success{
				width: max-content;
				padding: 10px;
				position: fixed;
				top: 10px;
				left: 100%;
				transition: left 0.4s;
				background-color: green;
				color: white;
				
			}
			#html-hint{
				position: fixed;
				left: calc(100% + 400px); 
				top: 0px;
				z-index: 5000;
			
			}
			#html-hint button{
				background-color: white;
				color: blue;
				border: none ; 
				padding: 10px;
				border: 2px solid blue;
				transform: translatex(-50%);
			}
			#html-hint button:hover{
				cursor: pointer;
				border-radius: 5px;
			}
			#wrapper div#topRightCorner {
				position: absolute;
				top: 5px;
				left: calc(100% - 58px);
				margin: 0;
				padding: 0;
			}
			#wrapper div#topRightCorner div{
				margin: 0;
				padding: 0;
			}
			#robotImage img{
				width: 48px;
				height: 48px;
				
			}
			#settingsImage{
				position: relative;
				background-color: white;
				cursor: pointer;
				width: 48px;
				height: 48px;
			
			}
			#escapeOrNot{
				background-color: rgb(234,182,118);
				margin: 0;
				padding: 0;
				position: absolute;
				top: 0;
				left: -220px;
				transform: scaleX(0);
				width: 200px;
				transform-origin: 100% 50%;
				transition: transform 0.4s;
				cursor: default;
				
				color: white; 
			}
			#escapeOrNotFlex{
				display: flex;
				gap: 10px;
				align-items: center;
			}
			#saveChanges{
				background-color: rgb(93, 226, 153);
				border: none;
				outline: none;
				padding: 5px;
				border-radius: 5px;
				margin: 5px 0px 5px 5px;
				cursor: pointer;
			}
			#chooseAgain{
				padding: 5px;
				border: 0;
				outline: 0;
				background-color:  rgb(25, 185, 229);
				color: white;
				cursor: pointer;
	
			}
		</style>
	</head>
	<body>
	<?php
	$category = 0;
	$key = "";
	$value = "";
	function getInfoForEntry($idNumber , &$key , &$value , &$category)
	{
		$servername = "db";
		$username = "root";
		$password = "sigurnaparola";
		$dbName = 'test';

		try {
		$pdo = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
		// set the PDO error mode to exception
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//echo"Connected successfully";
		}catch(PDOException $e){
		//echo "Connection failed: " . $e->getMessage();
		}

		$query = "select * from it where id = ?";


		$stmt = $pdo->prepare($query);
		$stmt->execute([$idNumber] );
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		$category = $result->category;
		if($category== NULL)
		{
			$category = 0 ; 
		}
		$key = $result->search;
		$value =  htmlspecialchars($result->result) ; 
		//echo '<script>';
		//echo "console.log('{$value}')";
	//	echo '</script>';
		
		
	}
	
	$mode = 'save';
	$recordID = "none";
	if(isset($_GET['id']))
		{
			$recordID = $_GET['id'];
			$mode = 'update';
			getInfoForEntry($_GET['id'] , $key , $value , $category);
		}
	?>
	
	<div id="html-hint"><button id="tearApart">escape special characters</button></div>
	<div id="success">Успешно добавихте нов ред </div>
	<div id="wrapper">
		<div id="choosenCategory">Category :</div>
		<div id="categoryWrapper"></div>
		<div id="">Keywords :</div>
		<div><textarea id="textarea1"><?php echo $key; ?></textarea></div>
		<div>Content :</div>
		<div id="textarea2wrapper" >
			<textarea id="textarea2"  ><?php echo $value; ?></textarea>
			<div id="mirror"></div>
		</div>
		<div><img   id="save" src="images/531889_backup_data_disk_diskette_download_icon.png"/></div>
		<div id="topRightCorner">
			<div id="robotImage"><img src="./images/robot.svg"></div>
			<div id="settingsImage">
				<img src="./images/Settings-icon.png">
				<div id="escapeOrNot">
					<div id="escapeOrNotFlex">
						<input type="checkbox" id="chechBoxForEscape" <?php if(!isset($_GET['id'])){echo 'checked=""';}?>  >
						<span>Да се ескейпват ли специалните символи</span>
					</div>
					<button id="saveChanges">Запази промените</button>
				</div>
			</div>
		</div>
		
		
	</div>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

	<?php echo "<script>window.savingMode = \"{$mode}\"</script>"; ?>
	<?php echo "<script>window.recordID = \"{$recordID}\"</script>"; ?>
	<?php echo "<script>window.category = \"{$category}\"</script>"; ?>
	<script>
	
	
	var hintVisible = false;
	
	document.getElementById("save").addEventListener('click' , prepareData);
	
	function showSuccessMessage(){
		let handle = document.getElementById("success");
		if(window.savingMode=='update')
		{
			document.getElementById("success").innerHTML = 'Успешно променихте записа';
		}
		let width = handle.offsetWidth ;
		let browserWidth = window.innerWidth ; 
		let leftOffset = browserWidth - width - 20; 
		handle.style.left = leftOffset+`px`;
		setTimeout(function(){
			document.getElementById("success").style.left = "100%";
			if(window.savingMode=='update')
			{
				window.location.replace("teachMeSomething.php");
			}
			 } , 3000);
	}
	function resetForm(){
		document.getElementById("textarea1").value = "";
		document.getElementById("textarea2").value = "";
		category = 0 ; 
	}
	
	function saveData(keywordsFromUser , contentFromUser , escape)
	{
		axios({method:'post' , url : 'saveKnowledge.php' , data: { categoryID : window.category , keywords : keywordsFromUser , content: contentFromUser , htmlSpecChars : escape , mode:window.savingMode , recordID: window.recordID }}).
		then(function(response)
		{
			console.log(response.data);
			
			if(response.data==1)
				{
					showSuccessMessage();
					resetForm();
				}
		});
	}
	
	
	function prepareData(){
		let keywords  = document.getElementById("textarea1").value;
		let content = document.getElementById("textarea2").value;

		let check = document.getElementById('chechBoxForEscape').checked;
		let escapeOrNot ;
		if(check==true)
			{
				escapeOrNot = 1 ; 
			}
			else{
					escapeOrNot = 0 ; 
			}
		
		
		
		if(category==0)
			{
					let choose = confirm("Няма избрана категория ! Сигурни ли сте че искате да продължите");
				
					if(choose == true)
						{
						// go ahead	
						saveData(keywords , content , escapeOrNot);
						}
						else{
							return ; 
						}
			}
			else{
				saveData(keywords , content , escapeOrNot);
			}
			
	
	}
	
	function getCategories(){
		axios({method: 'get' , url : 'readCategories.php'}).then(function(response){
			console.log(response.data);
			window.dataForHints = response.data;
			let parent = document.getElementById('categoryWrapper') ; 
			
			dataForHints.forEach(function(item){
				let child = document.createElement("div");
				child.innerHTML = item['category'];
				child.dataset.handle = 'hint';
				child.dataset.id = item['id'];
				child.dataset.category = item['category'];
				parent.appendChild(child);
			});
			
			
			getAllHints = document.querySelectorAll('[data-handle="hint"]');
			//console.log(getAllHints);
			const hintsArray = Array.from(getAllHints);
			hintsArray.forEach(function(item){
				item.addEventListener('click' , function(){
					category = this.dataset.id;
					document.getElementById("choosenCategory").innerHTML = `Category : ${this.dataset.category} <button id="chooseAgain"> Избери друга категория</button`;
					document.getElementById("chooseAgain").addEventListener('click' , function(){
					  document.getElementById("categoryWrapper").style.display = 'flex';
					});
					parent.style.display = 'none';
				});
			});
			
			}) 
		
		
	}
	function checkForSelection(event){
			
			selection = document.getSelection();
			
			
			if(document.getSelection().toString()=="")
			{
				
				return ; 
			}
			//console.log(document.getSelection().toString());
			console.log(selection);
		
			range = selection.getRangeAt(0);
			console.log(range);
			
			coodinates = range.getBoundingClientRect();
			
			// position hint
			let hint = document.getElementById("html-hint");
			hintx =coodinates.left + ((coodinates.right - coodinates.left) / 2 ) ;
			hint.style.left = hintx+'px';
			hinty = coodinates.top - 50;
			hint.style.top = hinty+'px';
			hintVisible = true ; 
			
			
			 		
        		
	}
	function tearApart()
	{
			// разпокъсай низа 
			let text = document.getElementById("textarea2").value;
			
			
			
			
			
			firstPart = text.substring(0 , cutStart);
			//console.log(firstPart);
			
		
			secondPart = text.substring(cutStart , cutEnd);
			thirdPart = text.substring(cutEnd, text.length);
			console.log(`${firstPart} | ${secondPart} | ${thirdPart}`);
			
			replaceSecondPart = secondPart.replace(/</g , '&lt;');
			replaceSecondPart = replaceSecondPart.replace(/>/g , '&gt;');
			
			console.log(replaceSecondPart);
			
			reassemble = `${firstPart}${replaceSecondPart}${thirdPart}`;
			console.log(reassemble);
			
			//selection.removeAllRanges();
			document.getElementById("textarea2").value = reassemble ; 
		//	console.log('inner html '+document.getElementById("textarea2").innerHTML);
		
		
			
			
			
			
	}
	
	function hideHint()
	{
			let hint = document.getElementById("html-hint");
			let posX = window.innerWidth + 400;
			let poxY = 0; 
			hint.style.left = posX+'px';
			hint.style.top = poxY+'px' ;
	}
	function showSettingsMenu()
	{
		
		document.getElementById('escapeOrNot').style.transform = "scaleX(1)";
		
	}
	
	
	function hideSettingsMenu(event)
	{
		event.stopPropagation();
		console.log('hide');
		document.getElementById('escapeOrNot').style.transform = "scaleX(0)";
		
		
	}
	function Mirror(event)
	{
		
		
		cutStart = 	this.selectionStart;
		cutEnd =     this.selectionEnd;
		const textAreaValue = document.getElementById('textarea2').value;
		console.log('text area: value '+textAreaValue);
		const mirror = document.getElementById('mirror');
		
	//	const textAreaScrollTop = document.getElementById('textarea2').scrollTop;
		//mirror.scrollTop = textAreaScrollTop;
	//	console.log('scroll From Top : '+textAreaScrollTop);
		
		
		mirror.innerHTML = "";
		let textNode = document.createTextNode(textAreaValue);
		mirror.appendChild(textNode);
		
		
		const textAreaScrollTop = document.getElementById('textarea2').scrollTop;
		mirror.scrollTop = textAreaScrollTop;
		console.log('scroll From Top : '+textAreaScrollTop);
		
		
		
		let mirrorTextNode = mirror.childNodes[0];
		console.log(mirrorTextNode);
		let range = new Range();
		range.setStart(mirrorTextNode , this.selectionStart);
		range.setEnd(mirrorTextNode , this.selectionEnd);
		console.log(range.getBoundingClientRect());
		coodinates = range.getBoundingClientRect();
			
		
		
		
			// position hint
			let hint = document.getElementById("html-hint");
			hintx =coodinates.left + ((coodinates.right - coodinates.left) / 2 ) ;
			hint.style.left = hintx+'px';
			hinty = coodinates.top - 50;
			hint.style.top = hinty+'px';
			
			hintVisible = true ; 
		
		
	}
	function tryToHideHint()
	{
		if(hintVisible == true)
			{
				console.log('should hide hint');
				hideHint();
				hintVisible = false;
				
			}
	}
	window.addEventListener('DOMContentLoaded' , 	getCategories ) ;
	document.querySelector('#settingsImage').addEventListener('click' , showSettingsMenu);
	document.getElementById('saveChanges').addEventListener('click' , hideSettingsMenu);
	
	//document.querySelector('#textarea2').addEventListener('mouseup' , checkForSelection);
	document.querySelector('#textarea2').addEventListener('select' , Mirror );
	document.querySelector('#textarea2').addEventListener('click' , tryToHideHint );
	document.getElementById('tearApart').addEventListener('click' , tearApart);
	//categoryWrapper   tearApart

	
		
	</script>
	</body>
</html>