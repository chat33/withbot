<?php
$servername = "db";
$username = "root";
$password = "sigurnaparola";
$dbName = 'test';

try {
$pdo = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
// set the PDO error mode to exception
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//echo"Connected successfully";
}catch(PDOException $e){
echo "Connection failed: " . $e->getMessage();
}

$query = "select id , category from categories" ;
$pdoStatement = $pdo->query($query);
//echo $pdoStatement->rowCount();
$result = $pdoStatement->fetchAll(PDO::FETCH_OBJ);

/*foreach($result as $indidivualResult)
{
	echo $indidivualResult->id; 
	echo PHP_EOL;
	echo  $indidivualResult->category; 
	echo PHP_EOL;
	
}
*/
$response = json_encode($result);
echo $response;

?>
